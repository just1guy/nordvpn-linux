/// //////////////////
//    Imports
const { ipcRenderer } = require('electron')
const $ = require('jquery')
const Store = require('electron-store')
/// //////////////////
//    Initialize objects
const store = new Store()
/// //////////////////
//    Get some elements
var sidePanel = document.getElementsByClassName('sidepanel')[0] // Some of these were added before I added jquery
var mainWindow = document.getElementsByClassName('main-window')[0]
var statusBar = document.getElementsByClassName('status-bar')[0]
var settings = document.getElementsByClassName('settings')[0]
var markers = $('.main-window img')
//    Scroll main window
$(mainWindow).scrollTop(store.get('mapScrollY', 340))
$(mainWindow).scrollLeft(store.get('mapScrollX', 340))
/// //////////////////
//    ipcRenderer events
ipcRenderer.on('connecting', (event, arg) => {
  // console.log(arg)
  $('.status-description').text('Connecting to ' + arg)
  connecting()
})

ipcRenderer.on('connected', (event, arg) => {
  $('.status-description').text('Connected to ' + arg)
  connected()
})

ipcRenderer.on('disconnected', (event, arg) => {
  // console.log(arg)
  $('.status-description').text(arg)
  disconnected()
})

ipcRenderer.on('get-status', (event, arg) => {
  ipcRenderer.send('nordexec', 'status')
})

ipcRenderer.on('version', (event, arg) => {
  $('#nordvpn-version').text(arg)
  $('#nordvpn-version').css('color', 'green')
})

ipcRenderer.on('trigger-settings-refresh', (event, arg) => {
  ipcRenderer.send('get-settings', '')
})

ipcRenderer.on('settings', (event, arg) => {
  if (arg[1] == null) { return }
  console.log(arg)
  parseSettings(arg)
})
/// //////////////////
//    Functions
function parseSettings (list) {
  var protocol = list[0][0].substring(10)
  var kill = list[1][0].substring(13)
  var cybersec = list[2][0].substring(10)
  var obfuscate = list[3][0].substring(11)
  var auto = list[4][0].substring(14)
  if (list[5][0] != null) {
    var dns = list[5][0].substring(5)
  }

  if (protocol === 'TCP') {
    $('#udp-check').prop('checked', false)
    $('#tcp-check').prop('checked', true)
  } else {
    $('#udp-check').prop('checked', true)
    $('#tcp-check').prop('checked', false)
  }

  if (kill === 'enabled') {
    $('#kill-check').prop('checked', true)
    $('#kill-label').text('on')
  } else {
    $('#kill-check').prop('checked', false)
    $('#kill-label').text('off')
  }

  if (cybersec === 'enabled') {
    $('#cyber-check').prop('checked', true)
    $('#cyber-label').text('on')
    $('.dns-text').val('')
    $('.dns-text').css('background-color', 'white')
  } else {
    $('#cyber-check').prop('checked', false)
    $('#cyber-label').text('off')
  }

  if (obfuscate === 'enabled') {
    $('#obfuscate-control i').prop('checked', true)
    $('#obfuscate-control span').text('on')
  } else {
    $('#obfuscate-control i').prop('checked', false)
    $('#obfuscate-control span').text('off')
  }

  if (auto === 'enabled') {
    $('#autoconnect').val(store.get('autoConnect').split('_').join(' '))
  } else {
    $('#autoconnect').val('None')
  }

  // console.log(dns)
  try {
    var dnsList = dns.split(' ')
    dnsList = dnsList.join('')
    dnsList = dnsList.split(',')
    var primary = dnsList[0].split('.')
    var secondary = dnsList[1].split('.')

    $('.dns-text').eq(0).val(secondary[0]) //   Reverse order of result from `nordvpn settings`
    $('.dns-text').eq(1).val(secondary[1])
    $('.dns-text').eq(2).val(secondary[2])
    $('.dns-text').eq(3).val(secondary[3])

    $('.dns-text').eq(4).val(primary[0])
    $('.dns-text').eq(5).val(primary[1])
    $('.dns-text').eq(6).val(primary[2])
    $('.dns-text').eq(7).val(primary[3])

    $('.dns-text').css('background-color', 'lightgreen')
  } catch(err) {

  }
}

function connecting () {
  $('.status-connection i').text('error')
  $('.status-connection').css('color', 'yellow')
  $('#con-status').text('Connecting')
}

function connected () {
  $('.status-connection i').text('check_circle')
  $('.status-connection').css('color', 'green')
  $('#con-status').text('Connected!')
  $('#qc').text('Disconnect')
  $('#qc').off()
  $('#qc').on('click', function () {
    ipcRenderer.send('nordexec', 'disconnect')
  })
}

function disconnected () {
  $('.status-connection i').text('error')
  $('.status-connection').css('color', 'red')
  $('#con-status').text('Unprotected')
  $('#qc').text('Quick Connect')
  $('#qc').off()
  $('#qc').on('click', function () {
    ipcRenderer.send('nordexec', 'connect')
  })
}
/// //////////////////
//    Event listeners for page elements
markers.click(function () {
  if ($(this).attr('title') == null) {
    return 0
  } else if ($(this).attr('title') === 'USA') {
    ipcRenderer.send('nordexec', 'connect United_States')
  } else if ($(this).attr('title') === 'UK') {
    ipcRenderer.send('nordexec', 'connect United Kingdom')
  } else {
    var name = $(this).attr('title').split(' ').join('_')
    ipcRenderer.send('nordexec', 'connect ' + name)
  }
})

$('.sp-item').click(function () {
  var name = $(this).children().eq(1).text().split(' ').join('_')
  ipcRenderer.send('nordexec', 'connect ' + name)
})

//    Window Controls
document.getElementById('closeWindow').addEventListener('click', function () {
  ipcRenderer.send('close-window', 1)
})

document.getElementById('maxWindow').addEventListener('click', function () {
  ipcRenderer.send('max-window', 1)
})

document.getElementById('minWindow').addEventListener('click', function () {
  ipcRenderer.send('min-window', 1)
})

document.getElementById('servers-button').addEventListener('click', function () {
  $(settings).css('display', 'none')
  $(statusBar).css('display', 'flex')
  $(mainWindow).css('display', 'block')
  $(sidePanel).css('display', 'flex')
  $('#servers-button').addClass('active')
  $('#settings-button').removeClass('active')
})

document.getElementById('settings-button').addEventListener('click', function () {
  $(settings).css('display', 'flex')
  $(statusBar).css('display', 'none')
  $(mainWindow).css('display', 'none')
  $(sidePanel).css('display', 'none')
  $('#servers-button').removeClass('active')
  $('#settings-button').addClass('active')
})

$('#dns-clear').click(function () {
  $('.dns-text').val('')
})

$('#dns-save').click(function () {
  var isProper = true
  $('.dns-text').each(function () {
    if ($(this).val() === '') {
      $(this).css('background-color', 'lightcoral')
      isProper = false
    } else { $(this).css('background-color', 'white') }
  })
  if (!isProper) { return }

  var primary = [$('.dns-text').eq(0).val(), $('.dns-text').eq(1).val(), $('.dns-text').eq(2).val(), $('.dns-text').eq(3).val()]
  var secondary = [$('.dns-text').eq(4).val(), $('.dns-text').eq(5).val(), $('.dns-text').eq(6).val(), $('.dns-text').eq(7).val()]
  ipcRenderer.send('nordexec', 'set dns ' + primary.join('.') + ' ' + secondary.join('.'))
  console.log(primary.join('.') + ' ' + secondary.join('.'))
})

$('#cyber-check').on('change', function () {
  if (!$(this).prop('checked')) {
    ipcRenderer.send('nordexec', 'set cybersec disabled')
  } else {
    ipcRenderer.send('nordexec', 'set cybersec enabled')
  }
})

$('#kill-check').on('change', function () {
  if (!$(this).prop('checked')) {
    ipcRenderer.send('nordexec', 'set killswitch disabled')
  } else {
    ipcRenderer.send('nordexec', 'set killswitch enabled')
  }
})

$('#obfuscate-check').on('change', function () {
  if (!$(this).prop('checked')) {
    ipcRenderer.send('nordexec', 'set obfuscate disabled')
  } else {
    ipcRenderer.send('nordexec', 'set obfuscate enabled')
  }
})

$('#udp-check').click(function () {
  ipcRenderer.send('nordexec', 'set protocol udp')
})
$('#tcp-check').click(function () {
  ipcRenderer.send('nordexec', 'set protocol tcp')
})

$('#autoconnect').change(function () {
  if ($(this).val() === 'None') {
    ipcRenderer.send('nordexec', 'set autoconnect off')
    store.set('autoConnect', 'disabled')
  } else if ($(this).val() === 'Fastest Server') {
    ipcRenderer.send('nordexec', 'set autoconnect on')
    store.set('autoConnect', 'Fastest Server')
  } else {
    store.set('autoConnect', $(this).val().split(' ').join('_'))
    ipcRenderer.send('nordexec', 'set autoconnect on ' + $(this).val().split(' ').join('_'))
  }
})

$('.main-window').on('scroll', function (event) {
  console.log()
  store.set('mapScrollX', $('.main-window').scrollLeft())
  store.set('mapScrollY', $('.main-window').scrollTop())
})

